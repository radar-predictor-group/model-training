import os
import numpy as np
import cv2
from PIL import Image, ImageDraw as ImageDraw, ImageFont as ImageFont
from keras.api._v2 import keras
from skimage.io import imread


def get_sorted_file_list(data_path):
    file_list = []
    for root, _, files in os.walk(data_path):
        for file in files:
            file_list.append(os.path.join(root, file))
    file_list.sort()
    return file_list


def add_title(image, title):
    draw = ImageDraw.Draw(image)
    # font = ImageFont.truetype('Pillow/Tests/fonts/FreeMono.ttf', 20)
    # draw.text((0, 0), title, fill=255, font=font)
    draw.text((0, 0), title, fill=255)
    return image


def as_image(np_array):
    arr = (255 * np_array).astype('uint8')[0].reshape(32, 32)
    image = Image.fromarray(arr, 'L')
    return image


def visual_compare(input_frames, expected_frame, predicted_frame):
    zoomed_size = (512, 512)
    expected_image = as_image(expected_frame)
    add_title(expected_image.resize(zoomed_size), title='Expected').show()
    predicted_image = as_image(predicted_frame)
    add_title(predicted_image.resize(zoomed_size), title='Predicted').show()


def load_images(image_dir='../data/images/processed/Sydney64k', max_images=100):
    filenames = get_sorted_file_list(image_dir)
    if max_images > 0:
        filenames = filenames[:max_images]
    # Load the images into a list
    images = []
    for file in filenames:
        if file.endswith(".png"):
            images.append(keras.preprocessing.image.load_img(file, color_mode='grayscale'))
    return images


def create_video(expected, predicted, input_width, input_height):
    num_images = len(expected)
    out = cv2.VideoWriter('output.mp4', cv2.VideoWriter_fourcc(*'mp4v'), 15, (2 * input_width, input_height))
    for i in range(num_images):
        # img = np.zeros((input_height, 2*input_width, 3), dtype=np.uint8)
        # img[:, :input_width, :] = cv2.cvtColor(array1[i], cv2.COLOR_GRAY2BGR)
        # img[:, input_width:, :] = cv2.cvtColor(array2[i], cv2.COLOR_GRAY2BGR)
        # out.write(img)
        img1 = np.uint8(255 * cv2.cvtColor(expected[i], cv2.COLOR_GRAY2BGR))
        img2 = np.uint8(255 * cv2.cvtColor(predicted[i], cv2.COLOR_GRAY2BGR))
        # combined_img = cv2.hconcat([img1, img2])
        combined_img = np.concatenate((img1, img2), axis=1, dtype=np.uint8)
        # cv2.putText(combined_img, 'Expected', (5, 10), cv2.FONT_HERSHEY_SIMPLEX, 0.2, (255, 255, 255), 2)
        # cv2.putText(combined_img, 'Predicted', (input_width + 5, 10), cv2.FONT_HERSHEY_SIMPLEX, 0.2, (255, 255, 255), 2)
        # # Draw a red line with a thickness of 3 pixels
        # cv2.line(combined_img, (input_width, 0), (input_width, input_height), (0, 0, 255), 3)
        out.write(combined_img)
    out.release()


def load_as_np_arr(filepath):
    arr = imread(filepath, as_gray=True).reshape(32, 32, 1).astype('float32') / 255
    return arr


def load_all_as_np_arr(max_files=100):
    DATA_PATH = '../data/images/processed/Sydney64k'
    file_list = get_sorted_file_list(DATA_PATH)
    if max_files > 0:
        file_list = file_list[:max_files]
    images = []
    for filename in file_list:
        images.append(load_as_np_arr(filename))
    images = np.array(images)
    return images
