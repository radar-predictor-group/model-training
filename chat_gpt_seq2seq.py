import numpy as np
import keras
from keras.layers import Input, LSTM, Dense
from keras.preprocessing.sequence import TimeseriesGenerator
from utils import load_all_as_np_arr


images = load_all_as_np_arr()

# Set the number of features in the input and output sequences
num_features = images.shape[1] * images.shape[2]

# # Convert the images to numpy arrays
# images = [np.array(image).reshape((-1, num_features)) for image in images]
#
# Normalize the images
images = keras.utils.normalize(images, axis=1)
images = images.reshape(-1, num_features)

total_frames = 7

num_known_frames = total_frames - 1

encoder_inputs = Input(shape=(None, num_features))
encoder = LSTM(128, return_state=True)
_, state_h, state_c = encoder(encoder_inputs)
encoder_states = [state_h, state_c]

decoder_inputs = Input(shape=(None, num_features))
decoder_lstm = LSTM(128, return_sequences=True, return_state=True)
decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
decoder_dense = Dense(num_features, activation='sigmoid')
decoder_outputs = decoder_dense(decoder_outputs)

model = keras.Model([encoder_inputs, decoder_inputs], decoder_outputs)
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

num_examples = len(images) - num_known_frames - 1

# Define the input and output sequences
X_train = np.zeros((num_examples, num_known_frames, num_features))
y_train = np.zeros((num_examples, num_features))
for i in range(num_examples):
    X_train[i, :, :] = images[i:i+num_known_frames, :]
    y_train[i, :] = images[i+num_known_frames, :]

# Train the model
model.fit(X_train, y_train, epochs=1)

X_test = np.zeros((1, num_known_frames, num_features))
X_test[0, :, :] = images[num_examples:num_examples + num_known_frames, :]
context_vector, state_h, state_c = encoder.predict(X_test)
predictions = model.predict([X_test, np.zeros((1, 1, num_features))])
