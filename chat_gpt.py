import numpy as np
from tensorflow import keras
from utils import visual_compare, load_images
import time


images = load_images()

# Convert the images to numpy arrays
image_array = [np.array(image) for image in images]

# Normalize the images
image_array = keras.utils.normalize(image_array, axis=1)

# Split the images into sequences of N frames
N = 7
sequences = []
for i in range(len(image_array) - N):
    sequences.append(image_array[i:i + N])

# Convert the sequences to numpy arrays
sequences = np.array(sequences)

# Split the sequences into input and output sequences
X = sequences[:, :-1]
y = sequences[:, -1]
channels = image_array.shape[1] * image_array.shape[2]
# X = X.reshape((-1, N-1, channels))
# y = y.reshape((-1, channels))

# Build a model to predict the next image in the sequence
model = keras.Sequential()
model.add(keras.layers.LSTM(128, input_shape=(N - 1, image_array.shape[1], image_array.shape[2])))
model.add(keras.layers.Dense(image_array.shape[1], activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# model = keras.Sequential()
# model.add(keras.layers.Conv2D(32, kernel_size=(3, 3), activation='relu',
#                               input_shape=(N - 1, image_array.shape[1], image_array.shape[2], image_array.shape[3])))
# model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
# model.add(keras.layers.Flatten())
# model.add(keras.layers.LSTM(128))
# model.add(keras.layers.Dense(image_array.shape[1], activation='sigmoid'))
# model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

start_time = time.perf_counter()

# Train the model
model.fit(X, y, epochs=10)

# Use the model to predict the next image in a sequence
predictions = model.predict(X[0])

end_time = time.perf_counter()
print("Fitting and predicting took {:0.2f} minutes".format((end_time - start_time) / 60))

print("Running visual comparison...")
visual_compare(None, X[N - 1], predictions[0])
