import numpy as np
import keras
# from keras.layers import Input, LSTM, Dense
from keras.layers.convolutional import Conv3D
from keras.preprocessing.sequence import TimeseriesGenerator
from keras.models import Sequential

from utils import load_images

images = load_images()

input_width = 32
num_features = input_width * input_width

# # Convert the images to numpy arrays
# images = [np.array(image).reshape((-1, num_features)) for image in images]
#
# Normalize the images
images = keras.utils.normalize(images, axis=1)
# images = images.reshape(-1, num_features)

NUM_INPUT_FRAMES = 6
TOTAL_NUM_FRAMES = NUM_INPUT_FRAMES + 1

model = Sequential()
model.add(Conv3D(1, (3, 3, 3), activation='relu', padding='same',
                 input_shape=(None, input_width, input_width, 1),
                 data_format='channels_last'))
model.compile(optimizer='adam', loss='mse')

generator = TimeseriesGenerator(images, images, length=NUM_INPUT_FRAMES, batch_size=32)

model.fit_generator(generator, epochs=10)

X_test = np.zeros((1, NUM_INPUT_FRAMES, num_features))
X_test[0, :, :] = images[num_examples:num_examples+NUM_INPUT_FRAMES, :]
context_vector, state_h, state_c = encoder.predict(X_test)
predictions = model.predict([X_test, np.zeros((1, 1, num_features))])