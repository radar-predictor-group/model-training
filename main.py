import argparse
import collections
import time

import numpy as np
from keras.layers.convolutional import Conv3D
from keras.models import Sequential
from keras.layers import LSTM, Dense
from keras.layers import ConvLSTM2D
from keras.layers import BatchNormalization

from utils import get_sorted_file_list, create_video, load_as_np_arr

DEFAULT_MAX_IMAGES = 1000
IMAGE_WIDTH = 32
IMAGE_HEIGHT = 32
NUM_INPUT_FRAMES = 6
NUM_OUTPUT_FRAMES = 1

CONV_LSTM_MODEL_TYPE = 'convlstm'
CONV_LSTM_BATCH_NORM_MODEL_TYPE = CONV_LSTM_MODEL_TYPE + '_batchnorm'
SIMPLE_MODEL_TYPE = 'conv3d'
WANDB_EXAMPLE_MODEL_TYPE = 'wandb_example'
CHAT_GPT = 'chatgpt'
MODEL_TYPES = [SIMPLE_MODEL_TYPE, CONV_LSTM_MODEL_TYPE, CONV_LSTM_BATCH_NORM_MODEL_TYPE,
               WANDB_EXAMPLE_MODEL_TYPE, CHAT_GPT]

RELU_ACT_FN = 'relu'
SIGMOID_ACT_FN = 'sigmoid'
ACT_FNS = [RELU_ACT_FN, SIGMOID_ACT_FN]


def build_model(input_width, model_type=SIMPLE_MODEL_TYPE, activation_fn=RELU_ACT_FN):
    if model_type in [CONV_LSTM_MODEL_TYPE, CONV_LSTM_BATCH_NORM_MODEL_TYPE]:
        use_batch_norm = model_type == CONV_LSTM_BATCH_NORM_MODEL_TYPE
        # We create a layer which take as input movies of shape
        # (n_frames, width, height, channels) and returns a movie
        # of identical shape.
        seq = Sequential()
        seq.add(ConvLSTM2D(filters=input_width, kernel_size=(3, 3),
                           input_shape=(None, input_width, input_width, 1),
                           padding='same', return_sequences=True))
        if use_batch_norm:
            seq.add(BatchNormalization())

        for _ in range(3):
            seq.add(ConvLSTM2D(filters=input_width, kernel_size=(3, 3),
                               padding='same', return_sequences=True))
            if use_batch_norm:
                seq.add(BatchNormalization())

        seq.add(Conv3D(filters=1, kernel_size=(3, 3, 3),
                       input_shape=(None, input_width, input_width, 1),
                       activation=activation_fn,
                       padding='same', data_format='channels_last'))
        seq.compile(loss='mean_squared_error', optimizer='adadelta')
        return seq
    elif model_type == SIMPLE_MODEL_TYPE:
        seq = Sequential()
        seq.add(Conv3D(1, (3, 3, 3), activation=activation_fn, padding='same',
                       input_shape=(NUM_INPUT_FRAMES, input_width, input_width, 1),
                       data_format='channels_last'))
        seq.compile(optimizer='adam', loss='mse')
    elif model_type == WANDB_EXAMPLE_MODEL_TYPE:
        from keras.layers import Lambda, Reshape, Permute

        def slice(x):
            return x[:, :, :, :, -1]

        seq = Sequential()
        seq.add(Reshape((NUM_INPUT_FRAMES, input_width, input_width, 1),
                        input_shape=(NUM_INPUT_FRAMES * 1, input_width, input_width)))
        seq.add(Permute((1, 2, 4, 3)))
        seq.add(Lambda(slice, input_shape=(NUM_INPUT_FRAMES, input_width, input_width, 1),
                       output_shape=(input_width, input_width, 1)))

        seq.compile(optimizer='adam', loss='mse')

    elif model_type == CHAT_GPT:
        seq = Sequential()
        seq.add(LSTM(units=64, input_shape=(NUM_INPUT_FRAMES, input_width, input_width)))
        seq.add(Dense(units=32, activation='relu'))
        seq.add(Dense(units=1))
        # compile the model
        seq.compile(loss='mean_squared_error', optimizer='adam')

    return seq


def load_data(max_images):
    DATA_PATH = '../data/images/processed/Sydney64k'
    file_list = get_sorted_file_list(DATA_PATH)

    # we do sliding frames, 6 inputs and 1 output. So,
    # input_frames[0] = f0, f1, f2, f3, f4, f5
    # expected_frames[0] = f6
    # input_frames[1] = f1, f2, f3, f4, f5, f6
    # expected_frames[1] = f7
    # so the max number of samples we can make is max_images - 6
    max_images = min(max_images, len(file_list)) if max_images > 0 else len(file_list)
    num_samples = max_images - NUM_INPUT_FRAMES - 1
    NUM_IMAGES_PER_SAMPLE = NUM_INPUT_FRAMES + NUM_OUTPUT_FRAMES
    if num_samples >= NUM_IMAGES_PER_SAMPLE:
        input_frames = np.zeros((num_samples, NUM_INPUT_FRAMES, IMAGE_WIDTH, IMAGE_HEIGHT, 1), dtype=np.float32)
        expected_frames = np.zeros((num_samples, NUM_OUTPUT_FRAMES, IMAGE_WIDTH, IMAGE_HEIGHT, 1), dtype=np.float32)
        loaded_images = collections.deque(maxlen=NUM_IMAGES_PER_SAMPLE)
        # now prefill the loaded_images circular buffer
        for image_idx in range(NUM_IMAGES_PER_SAMPLE):
            loaded_images.append(load_as_np_arr(file_list[image_idx]))
        # and finally start adding them to the data arrays to use for training
        image_idx = NUM_IMAGES_PER_SAMPLE
        for sample_idx in range(num_samples):
            for frame_idx in range(NUM_INPUT_FRAMES):
                input_frames[sample_idx, frame_idx] = loaded_images[frame_idx]
            expected_frames[sample_idx, 0] = loaded_images[NUM_INPUT_FRAMES]
            # now progress the loaded_images to add the next frame
            loaded_images.append(load_as_np_arr(file_list[image_idx]))
            image_idx += 1
        return input_frames, expected_frames
    return None, None


def get_parsed_args():
    parser = argparse.ArgumentParser(
        description='Train a model on the default data and so some quick predictions')
    parser.add_argument('--epochs', '-e', default=1, type=int, help='the number of epochs to train for')
    parser.add_argument('--num_images', '-n', default=DEFAULT_MAX_IMAGES, type=int,
                        help='the maximum number of images to train on. 0 means use the whole set')
    parser.add_argument('--model_type', '-m', default=SIMPLE_MODEL_TYPE, type=str,
                        choices=MODEL_TYPES, help='Specify the model type to use')
    parser.add_argument('--activation_fn', '-a', default=RELU_ACT_FN, type=str,
                        choices=ACT_FNS, help='Specify the activation function to use in the last hidden layer')
    return parser.parse_args()


def main():
    args = get_parsed_args()
    max_images = args.num_images
    num_epochs = args.epochs
    model_type = args.model_type
    activation_fn = args.activation_fn

    print("Training with {} images for {} epochs".format(max_images, num_epochs))
    print("Loading data...")
    input_frames, expected_frames = load_data(max_images=max_images)
    print("Building model...")
    model = build_model(input_width=32, model_type=model_type, activation_fn=activation_fn)
    print("Fitting model...")

    input_frames = np.asarray(input_frames)
    expected_frames = np.asarray(expected_frames)
    model.fit(input_frames, expected_frames, batch_size=10, epochs=num_epochs, validation_split=0.1)

    # feed it with the first group of frames and then
    # predict the next frame
    print("Making predictions...")
    # todo - why is my model output 6 frames as well? I want to predict a single frame
    predictions = model.predict(input_frames, verbose=1)
    # print("Running visual comparison...")
    # visual_compare(input_frames[0], expected_frames[0], predictions[0])
    expected_frames = expected_frames[:, 0, ]
    predictions = predictions[:, 0, ]
    create_video(expected_frames, predictions, IMAGE_WIDTH, IMAGE_HEIGHT)


if __name__ == '__main__':
    start_time = time.perf_counter()
    main()
    end_time = time.perf_counter()
    print("Main took {:0.2f} minutes".format((end_time - start_time) / 60))
